(ns io.gitlab.canterville.tammany
  (:require [clojure.data.xml :as xml]
            [io.gitlab.canterville.tammany.atom :as atm]
            [io.gitlab.canterville.tammany.rss :as rss]
            [clojure.string :as str]
            [clojure.java.io :as io]
            [clojure.spec.alpha :as s])
  (:import [java.io StringReader]))

;; TODO: cljs compatibility through https://github.com/funcool/tubax ?

(require [(xml/uri-symbol "http://www.w3.org/2005/Atom") :as-alias 'atom-1-ns]
         [(xml/uri-symbol "http://purl.org/rss/1.0/") :as-alias 'rss-1-ns]
         [(xml/uri-symbol "http://purl.org/atom/ns#") :as-alias 'atom-03-ns])

(s/def ::published inst?)
(s/def ::authors (s/coll-of string?))
(s/def ::image string?)
(s/def ::generator string?)
(s/def ::link string?)
(s/def ::language string?)
(s/def ::content string?)
(s/def ::type string?)
(s/def ::title (s/keys :req-un [::content] :opt-un [::type]))
(s/def ::copyright (s/keys :req-un [::content] :opt-un [::type]))
(s/def ::feed-link string?)
(s/def ::updated inst?)
(s/def ::guid string?)
(s/def ::length integer?)
(s/def ::href string?)
(s/def ::enclosures (s/coll-of (s/keys :req-un [::href ::length ::type])))
(s/def ::summary (s/keys :req-un [::content ::type]))
(s/def ::id string?)
(s/def ::uri string?)
(s/def ::source (s/keys :req-un [::title ::uri] :opt-un [::id]))
(s/def ::description (s/keys :req-un [::content] :opt-un [::type]))
(s/def ::entries (s/coll-of (s/keys :req-un [::description]
                                    :opt-un [::authors
                                             ::enclosures
                                             ::guid
                                             ::link
                                             ::published
                                             ::source
                                             ::summary
                                             ::title
                                             ::updated])))
(s/def ::feed (s/keys :req-un [::entries]
                      :opt-un [::link
                               ::description
                               ::title
                               ::authors
                               ::copyright
                               ::feed-link
                               ::generator
                               ::image
                               ::language
                               ::published
                               ::updated]))

(defn dispatch-extractor [dom]
  (cond
    (= (:tag dom) ::atom-1-ns/feed) {:metadata {::format :atom
                                                ::label "Atom"
                                                ::version "1.0"}
                                     :extractor atm/extract-feed-1}
    (= (:tag dom) ::atom-03-ns/feed) {:metadata {::format :atom
                                                 ::label "Atom"
                                                 ::version "0.3"}
                                     :extractor atm/extract-feed-03}
    (= (:tag (first (filter #(= (:tag %) ::rss-1-ns/channel) (:content dom))))
       ::rss-1-ns/channel) {:metadata {::format :rss
                                       ::label "RDF Site Summary"
                                       ::version "1.0"}
                            :extractor rss/extract-feed-1}
    (and (= (:tag dom) :rss)
         (= (get-in dom [:attrs :version]) "2.0")) {:metadata {::format :rss
                                                               ::label "Really Simple Syndication"
                                                               ::version "2.0"}
                                                    :extractor rss/extract-feed-2}
    (and (= (:tag dom) :rss)
         (= (get-in dom [:attrs :version]) "0.92")) {:metadata {::format :rss
                                                                ::label "Really Simple Syndication"
                                                                ::version "0.92"}
                                                     :extractor rss/extract-feed-2}
    (and (= (:tag dom) :rss)
         (= (get-in dom [:attrs :version]) "0.91")) {:metadata {::format :rss
                                                                ::label "Really Simple Syndication"
                                                                ::version "0.91"}
                                                     :extractor rss/extract-feed-2}
    (and (= (:tag dom) :rss)
         (= (get-in dom [:attrs :version]) "0.90")) {:metadata {::format :rss
                                                                ::label "Really Simple Syndication"
                                                                ::version "0.90"}
                                                     :extractor rss/extract-feed-2}
    :else nil))

(defn- apply-procs
  [procs data]
  (persistent!
   (reduce
    (fn [acc [k {:keys [proc]}]]
      (if proc
        (if-let [c (proc data)]
          (if-not (and (coll? c) (empty? c))
            (assoc! acc k c)
            acc)
          acc)
        acc)) (transient {}) procs)))

(defn- raw-normalize-feed
  [feed-procs entry-procs entries-fn feed]
  (let [feed-data (apply-procs feed-procs feed)
        entries (entries-fn feed)
        entries-data (mapv (fn [e] (apply-procs entry-procs e)) entries)]
    (with-meta
      (assoc feed-data :entries entries-data)
      (assoc (meta feed) ::raw feed))))

(def normalize-atom (partial raw-normalize-feed atm/normalize-feed-procs atm/normalize-entry-procs :entries))

(def normalize-rss (partial raw-normalize-feed rss/normalize-feed-procs rss/normalize-item-procs :items))

#?(:clj (defn parse-feed [str-or-rdr]
          (if (string? str-or-rdr)
            (xml/parse (StringReader. str-or-rdr))
            (xml/parse str-or-rdr)))
   :cljs (defn parse-feed [str-or-dom]
           (if (string? str-or-dom)
             (xml/parse-str str-or-dom)
             (xml/element-data (.-documentElement str-or-dom)))))

(defn extract-feed [dom]
  (def dom dom)
  (let [{:keys [metadata extractor]} (dispatch-extractor dom)]
    (with-meta (extractor dom) metadata)))

(defn normalize-feed [feed]
  (let [{::keys [format]} (meta feed)]
    (case format
      :rss (normalize-rss feed)
      :atom (normalize-atom feed)
      (throw (ex-info "Could not determine feed type" (dissoc (meta feed) ::raw))))))

(defn process-feed
  "Given either a string of xml, a reader (in clj) or a dom (in cljs), parses it and returns a normalized feed structure,
  with the exact result of parsing available as metadata on the feed as ::raw
  with ::version, ::label and ::format documenting the result."
  [str-or-rdr-or-dom]
  (-> str-or-rdr-or-dom
      (parse-feed)
      (extract-feed)
      (normalize-feed)))

(s/fdef process-feed
  :args #?(:clj (s/or :reader #(instance? java.io.Reader %) :string string?)
           :cljs (s/or :dom #(instance? Object %) :string string?))
  :ret ::feed)

(comment
  #?(:clj
     (def asample (-> "dev-resources/feeds/sample-atom-1.xml" (io/reader) (parse-feed)))
     (def a03sample (-> "dev-resources/feeds/sample-atom-03.xml" (io/reader) (parse-feed)))
     (def csample (-> "dev-resources/feeds/clojure.xml" (io/reader) (parse-feed)))
     (def r2sample (-> "dev-resources/feeds/sample-rss-2.xml" (io/reader) (parse-feed)))
     (def r092sample (-> "dev-resources/feeds/sample-rss-092.xml" (io/reader) (parse-feed)))
     (def r091sample (-> "dev-resources/feeds/sample-rss-091.xml" (io/reader) (parse-feed)))
     (def r1ssample (-> "dev-resources/feeds/sample-rss-1-simple.rdf" (io/reader) (parse-feed)))
     (def r1dsample (-> "dev-resources/feeds/sample-rss-1-dublin.xml" (io/reader) (parse-feed)))

     (require '[spec-provider.provider :as sp])

     (defn all-feeds []
       (let [feeds (file-seq (io/file "dev-resources/feeds"))]
         (for [feed (rest feeds)]
           (-> feed (io/reader)  (process-feed)))))

     (defn infer-specs []
       (sp/pprint-specs (sp/infer-specs (all-feeds) ::feed) 'io.gitlab.canterville.tammany 's))

     (defn check-specs []
       (for [feed (all-feeds)]
         (s/explain ::feed feed)))))
