(ns io.gitlab.canterville.tammany.atom
  "See https://xml2rfc.tools.ietf.org/public/rfc/html/rfc4287.html for spec. Only version 1.0 for now."
  (:require [clojure.data.xml :as xml]
            [medley.core :as med]
            [clojure.string :as str]
            [io.gitlab.canterville.tammany.common :as com])
  #?(:clj (:import [java.time.format DateTimeFormatter]
                   [java.time OffsetDateTime]
                   [java.util Date])))

(def ns-uri  {:atom-1  "http://www.w3.org/2005/Atom"
              :atom-03 "http://purl.org/atom/ns#"})

(def text-costructs-conv {["text/plain" "escaped"] "text"
                          ["text/html"  "escaped"] "html"
                          ["application/xhtml+xml" "xml"] "xhtml"
                          ["application/xml" "xml"] "application/xml"
                          ["application/mathml+xml" "xml"] "application/mathml+xml"
                          ["image/svg+xml" "xml"] "image/svg+xml"
                          ["image/png" "base64"] "image/png"
                          ["application/pdf" "base64"] "application/pdf"})

(defn parse-date [s]
  #?(:clj (let [formatter DateTimeFormatter/ISO_OFFSET_DATE_TIME]
            (try
              (Date/from (.toInstant (OffsetDateTime/parse s formatter)))
              (catch Exception _
                nil)))
     :cljs s))

(defn extract-text-construct [n]
  (let [typ (-> n :attrs :type)
        mode (-> n :attrs :mode)
        content (:content n)
        conv-type (if mode
                    (text-costructs-conv [typ mode])
                    typ)]
    (when (seq content)
      {:type (or conv-type "text")
       :content (str/trim (str/join "\n" content))})))

(defn extract-link [n]
  (let [{:keys [href rel type hreflang title length]} (:attrs n)]
    (-> nil
        (med/assoc-some :href href :rel (if rel (keyword rel) :alternate)
                        :type type :href-lang hreflang :title title
                        :length (com/try-parse-long length)))))

(defn extract-category [n]
  (let [{:keys [term scheme label]} (:attrs n)]
    (-> nil
        (med/assoc-some :term term :scheme scheme :label label))))

(defn extract-generator [n]
  (let [{:keys [uri url version]} (:attrs n)
        label (some-> (:content n) (first) (str/trim))]
    (-> nil
        (med/assoc-some :label label :version version
                        :uri (or uri url) ;; 0.3 spec
                        ))))

(def person-procs {:name  {:label       :name
                           :proc        com/extract-string
                           :cardinality :one}
                   :uri   {:label       :uri
                           :proc        com/extract-string
                           :cardinality :one}
                   :url   {:label       :uri ;; 0.3 spec
                           :proc        com/extract-string
                           :cardinality :one}
                   :email {:label       :email
                           :proc        com/extract-string
                           :cardinality :one}})

(def extract-person-1 (partial com/extract-data (com/add-map-ns (:atom-1 ns-uri) person-procs)))

(def extract-person-03 (partial com/extract-data (com/add-map-ns (:atom-03 ns-uri) person-procs)))

(def common-procs {:author      {:label :authors
                                 :proc  #(or (extract-person-1 %) (extract-person-03 %))}
                   :category    {:label :categories
                                 :proc  extract-category}
                   :contributor {:label :contributors
                                 :proc  #(or (extract-person-1 %) (extract-person-03 %))}
                   :id          {:label       :id
                                 :proc        com/extract-string
                                 :cardinality :one}
                   :link        {:label :links
                                 :proc  extract-link}
                   :copyright   {:label       :rights ;; O.3 spec
                                 :proc        extract-text-construct
                                 :cardinality :one}
                   :rights      {:label       :rights
                                 :proc        extract-text-construct
                                 :cardinality :one}
                   :title       {:label       :title
                                 :proc        extract-text-construct
                                 :cardinality :one}
                   :updated     {:label       :updated
                                 :proc        (comp parse-date com/extract-string)
                                 :cardinality :one}})

(declare extract-entry-1)
(declare extract-entry-03)

(def entry-procs (merge common-procs
                        {:content   {:label       :content
                                     :proc        extract-text-construct
                                     :cardinality :one}
                         :published {:label       :published
                                     :proc        (comp parse-date com/extract-string)
                                     :cardinality :one}
                         :issued    {:label       :published ;; 0.3 spec
                                     :proc        (comp parse-date com/extract-string)
                                     :cardinality :one}
                         :source    {:label       :source
                                     :proc        #(or (extract-entry-1 %) (extract-entry-03 %))
                                     :cardinality :one}
                         :summary   {:label       :summary
                                     :proc        extract-text-construct
                                     :cardinality :one}}))

(def extract-entry-1 (partial com/extract-data (com/add-map-ns (:atom-1 ns-uri) entry-procs)))

(def extract-entry-03 (partial com/extract-data (com/add-map-ns (:atom-03 ns-uri) entry-procs)))

(def feed-procs (merge common-procs
                       {:generator {:label       :generator
                                    :proc        extract-generator
                                    :cardinality :one}
                        :icon      {:label       :icon
                                    :proc        com/extract-string
                                    :cardinality :one}
                        :logo      {:label       :logo
                                    :proc        com/extract-string
                                    :cardinality :one}
                        :subtitle  {:label       :subtitle
                                    :proc        extract-text-construct
                                    :cardinality :one}
                        :tagline   {:label       :subtitle ;; 0.3 spec
                                    :proc        extract-text-construct
                                    :cardinality :one}
                        :entry     {:label :entries
                                    :proc  #(or (extract-entry-1 %) (extract-entry-03 %))}}))


(def extract-feed-1  (partial com/extract-data (com/add-map-ns (:atom-1 ns-uri) feed-procs)))

(def extract-feed-03  (partial com/extract-data (com/add-map-ns (:atom-03 ns-uri) feed-procs)))

(defn normalize-person [{:keys [name email uri url]}]
  (or name uri url email))

(defn normalize-category [{:keys [term label]}]
  (when (or term label)
    (str/join " - " (remove str/blank? [term label]))))

(def normalize-feed-procs
  {:title       {:proc :title}
   :description {:proc (fn [f] (or (:subtitle f)
                                   (:tagline f)))}
   :link        {:proc (fn [{:keys [links]}]
                         (let [link (or (first (filter #(= :alternate (:rel %)) links))
                                        (first (filter #(nil? (:rel %)) links)))]
                           (:href link)))}
   :feed-link   {:proc (fn [{:keys [links]}]
                         (let [link (first (filter #(= :self (:rel %)) links))]
                           (:href link)))}
   :updated     {:proc :updated}
   :authors     {:proc (fn [f] (mapv normalize-person (:authors f)))}
   :language    {}
   :image       {:proc :logo}
   :copyright   {:proc (fn [f] (or (:rights f)
                                   (:copyright f)))}
   :generator   {:proc (fn [f] (let [{:keys [label version uri]} (:generator f)]
                                 (when (or label uri)
                                   (str/join " - " (remove str/blank? [label version uri])))))}
   :categories  {:proc (fn [f] (mapv normalize-category (:categories f)))}})

(def normalize-entry-procs
  {:title       {:proc :title}
   :description {:proc :content}
   :summary     {:proc :summary}
   :link        {:proc (fn [{:keys [links]}]
                         (let [link (or (first (filter #(= :alternate (:rel %)) links))
                                        (first (filter #(nil? (:rel %)) links)))]
                           (:href link)))}
   :updated     {:proc (fn [f] (or (:modified f)
                                   (:updated f)))}
   :published   {:proc (fn [f] (or (:published f)
                                   (:issued f)))}
   :authors     {:proc (fn [f] (mapv normalize-person (:authors f)))}
   :guid        {:proc :id}
   :categories  {:proc (fn [f] (mapv normalize-category (:categories f)))}
   :source      {:proc (fn [f] (let [{:keys [title links id]} (:source f)
                                     uri (or (first (filter #(= :self (:rel %)) links))
                                             (first (filter #(nil? (:rel %)) links)))]
                                 (med/assoc-some nil :title title :uri (:href uri) :id id)))}
   :enclosures  {:proc (fn [{:keys [links]}]
                         (->> links
                              (filter #(= (:rel %) :enclosure))
                              (map #(dissoc % :rel))))}})
