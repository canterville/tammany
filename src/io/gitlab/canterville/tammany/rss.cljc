(ns io.gitlab.canterville.tammany.rss
  (:require [io.gitlab.canterville.tammany.common :as com]
            [clojure.string :as str]
            [medley.core :as med]
            [clojure.data.xml :as xml])
  #?(:clj (:import [java.time.format DateTimeFormatter]
                   [java.time ZonedDateTime]
                   [java.util Date])))

;; See https://validator.w3.org/feed/docs/rss2.html for spec

(require [(xml/uri-symbol "http://purl.org/rss/1.0/") :as-alias 'rss-1-ns])

(def ns-uri {:rss-1 "http://purl.org/rss/1.0/"
             :dc    "http://purl.org/dc/elements/1.1/"})

(defn parse-date [s]
  #?(:clj (try
            (let [formatter DateTimeFormatter/RFC_1123_DATE_TIME]
              (Date/from (.toInstant (ZonedDateTime/parse s formatter))))
            (catch Exception _
              nil))
     :cljs s))

(defn extract-category [n]
  (let [domain (some-> n :attrs :domain)
        title (first (:content n))]
    (-> {}
        (med/assoc-some :title (some-> title str/trim)
                        :domain domain))))

(def common-procs {:pubDate     {:label       :pub-date
                                 :proc        (comp parse-date com/extract-string)
                                 :cardinality :one}
                   :title       {:label       :title
                                 :proc        com/extract-string
                                 :cardinality :one}
                   :link        {:label       :link
                                 :proc        com/extract-string
                                 :cardinality :one}
                   :description {:label       :description
                                 :proc        com/extract-string
                                 :cardinality :one}
                   :category    {:label :categories
                                 :proc  extract-category}})

(defn extract-source [n]
  (let [url (some-> n :attrs :url)
        title (first (:content n))]
    (-> {}
        (med/assoc-some :title (some-> title str/trim)
                        :url url))))

(defn extract-guid [n]
  (let [pl (some-> n :attrs :isPermalink)
        guid (first (:content n))]
    (-> {}
        (med/assoc-some :id (some-> guid str/trim)
                        :permalink? pl))))

(defn extract-enclosure [n]
  (let [{:keys [url length type]} (:attrs n)]
    (-> {}
        (med/assoc-some :url url
                        :length (com/try-parse-long length)
                        :type type))))

(def item-procs-2 (merge common-procs
                         {:author    {:label       :author
                                      :proc        com/extract-string
                                      :cardinality :one}
                          :comments  {:label       :comments
                                      :proc        com/extract-string
                                      :cardinality :one}
                          :enclosure {:label       :enclosure
                                      :proc        extract-enclosure
                                      :cardianlity :one}
                          :guid      {:label       :guid
                                      :proc        extract-guid
                                      :cardinality :one}
                          :source    {:label       :source
                                      :proc        extract-source
                                      :cardinality :one}}))

(def dublin-core-procs {:title       {:label       :title
                                      :proc        com/extract-string
                                      :cardinality :one}
                        :creator     {:label       :creator
                                      :proc        com/extract-string
                                      :cardinality :one}
                        :subject     {:label       :category
                                      :proc        (comp (fn [s] (when s {:title s})) com/extract-string)}
                        :description {:label       :description
                                      :proc        com/extract-string
                                      :cardinality :one}
                        :publisher   {:label       :publisher
                                      :proc        com/extract-string
                                      :cardinality :one}
                        :contributor {:label       :contributor
                                      :proc        com/extract-string}
                        :date        {:label       :date
                                      :proc        (comp parse-date com/extract-string)
                                      :cardinality :one}
                        :type        {:label       :type
                                      :proc        com/extract-string
                                      :cardinality :one}
                        :identifier  {:label       :identifier
                                      :proc        com/extract-string
                                      :cardinality :one}
                        :source      {:label       :source
                                      :proc        (comp (fn [s] (when s {:title s})) com/extract-string)
                                      :cardinality :one}
                        :language    {:label       :language
                                      :proc        com/extract-string
                                      :cardinality :one}
                        :relation    {:label       :relation
                                      :proc        com/extract-string
                                      :cardinality :one}
                        :coverage    {:label       :coverage
                                      :proc        com/extract-string
                                      :cardinality :one}
                        :rights      {:label       :copyright
                                      :proc        com/extract-string
                                      :cardinality :one}})

(def extract-item-2 (partial com/extract-data item-procs-2))

(def extract-item-1 (partial com/extract-data (merge (com/add-map-ns (:rss-1 ns-uri) common-procs)
                                                     (com/add-map-ns (:dc ns-uri) dublin-core-procs))))

(defn extract-text-input [n]
  (let [pl (some-> n :attrs :isPermalink)
        guid (first (:content n))]
    (-> {}
        (med/assoc-some :id (some-> guid str/trim)
                        :permalink? pl))))

(def text-input-procs (merge (select-keys common-procs [:title :description :link])
                             {:name {:label       :name
                                     :proc        com/extract-string
                                     :cardinality :one}}))

(def image-procs (merge (select-keys common-procs [:title :description :link])
                        {:url    {:label       :url
                                  :proc        com/extract-string
                                  :cardinality :one}
                         :width  {:label       :width
                                  :proc        com/extract-long
                                  :cardinality :one}
                         :height {:label       :height
                                  :proc        com/extract-long
                                  :cardinality :one}}))

(defn extract-skip-hours [n]
  (let [hours (filter #(= (:tag %) :hour) (:content n))]
    (into #{} (map (comp com/try-parse-long first :content) hours))))

(defn extract-skip-days [n]
  (let [days (filter #(= (:tag %) :day) (:content n))]
    (into #{} (map (comp first :content) days))))

(defn extract-cloud [n]
  (let [{:keys [domain port path registerProcedure protocol]} (:attrs n)]
    (-> {}
        (med/assoc-some :domain domain
                        :port (com/try-parse-long port)
                        :path path
                        :register-procedure registerProcedure
                        :protocol protocol))))

(def channel-procs-2 {:language       {:label       :language
                                       :proc        com/extract-string
                                       :cardinality :one}
                      :copyright      {:label       :copyright
                                       :proc        com/extract-string
                                       :cardinality :one}
                      :managingEditor {:label       :managing-editor
                                       :proc        com/extract-string
                                       :cardinality :one}
                      :webMaster      {:label       :webmaster
                                       :proc        com/extract-string
                                       :cardinality :one}
                      :ttl            {:label       :ttl
                                       :proc        com/extract-long
                                       :cardinality :one}
                      :cloud          {:label       :cloud
                                       :proc        extract-cloud
                                       :cardinality :one}
                      :skipHours      {:label       :skip-hours
                                       :proc        extract-skip-hours
                                       :cardinality :one}
                      :skipDays       {:label       :skip-days
                                       :proc        extract-skip-days
                                       :cardinality :one}
                      :rating         {:label       :rating
                                       :proc        com/extract-string
                                       :cardinality :one}
                      :generator      {:label       :generator
                                       :proc        com/extract-string
                                       :cardinality :one}
                      :docs           {:label       :docs
                                       :proc        com/extract-string
                                       :cardinality :one}
                      :lastBuildDate  {:label       :last-build-date
                                       :proc        (comp parse-date com/extract-string)
                                       :cardinality :one}
                      :image          {:label       :image
                                       :proc        (partial com/extract-data image-procs)
                                       :cardinality :one}
                      :textInput      {:label       :text-input
                                       :proc        (partial com/extract-data text-input-procs)
                                       :cardinality :one}
                      :item           {:label :items
                                       :proc  extract-item-2}})

(def channel-procs-1 {:image          {:label       :image
                                       :proc        (partial com/extract-data (com/add-map-ns (:rss-1 ns-uri) image-procs))
                                       :cardinality :one}
                      :textInput      {:label       :text-input
                                       :proc        (partial com/extract-data (com/add-map-ns (:rss-1 ns-uri) text-input-procs))
                                       :cardinality :one}
                      :item           {:label :items
                                       :proc  extract-item-1}})

(def extract-channel-2 (partial com/extract-data (merge common-procs channel-procs-2)))

(def extract-channel-1 (partial com/extract-data (merge (com/add-map-ns (:rss-1 ns-uri) common-procs)
                                                        (com/add-map-ns (:dc ns-uri) dublin-core-procs))))

(def extract-out-of-channel-1 (partial com/extract-data (com/add-map-ns (:rss-1 ns-uri) channel-procs-1)))

(defn extract-feed-2 [dom]
  (let [tops (:content dom)
        channel-node (first (filter #(= (:tag %) :channel) tops))]
    (extract-channel-2 channel-node)))

(defn extract-feed-1 [dom]
  (let [tops (:content dom)
        channel-node (first (filter #(= (:tag %) ::rss-1-ns/channel) tops))
        channel-data (extract-channel-1 channel-node)
        data (extract-out-of-channel-1 dom)]
    (merge channel-data data)))

(def normalize-feed-procs
  {:title       {:proc (fn [f] (when-let [title (:title f)]
                                 {:content title}))}
   :description {:proc (fn [f] (when-let [description (:description f)]
                                 {:content description}))}
   :link        {:proc :link}
   :feed-link   {}
   :updated     {:proc (fn [f] (or (:last-build-date f)
                                   (:date f)))}
   :published   {:proc :pub-date}
   :authors     {:proc (fn [f] (into [] (remove empty? [(:managing-editor f)
                                                        (:webmaster f)
                                                        (:author f)
                                                        (:creator f)
                                                        (:publisher f)
                                                        (:contributor f)])))}
   :language    {:proc :language}
   :image       {:proc (comp :url :image)}
   :copyright   {:proc (fn [f] (let [c (or (:copyright f)
                                           (:rights  f))]
                                 (when c {:content c})))}
   :generator   {:proc :generator}
   :categories  {:proc (fn [f] (into [] (remove empty? (map :title [(:category f)
                                                                    (:subject f)]))))}})

(def normalize-item-procs
  {:title       {:proc (fn [f] (when-let [title (:title f)]
                                 {:content title}))}
   :description {:proc (fn [f] (when-let [description (:description f)]
                                 {:content description}))}
   :link        {:proc :link}
   :updated     {:proc :date}
   :published   {:proc (fn [f] (or (:pub-date f)
                                   (:date f)))}
   :authors     {:proc (fn [f] (into [] (remove empty? [(:author f)
                                                        (:creator f)
                                                        (:contributor f)
                                                        (:publisher f)])))}
   :guid        {:proc (comp :id :guid)}
   :image       {:proc (comp :url :image)}
   :source      {:proc (fn [f] (when-let [{:keys [title url]} (:source f)]
                                 (med/assoc-some nil :title {:content title} :uri url)))}
   :categories  {:proc (fn [f] (into [] (remove empty? (map :title [(:category f)
                                                                    (:subject f)]))))}
   :enclosures  {:proc (fn [f] (let [{:keys [url length type]} (:enclosure f)]
                                 (when url
                                   [(med/assoc-some nil :href url :type type :length length)])))}})
