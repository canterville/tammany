(ns io.gitlab.canterville.tammany.common
  (:require [clojure.string :as str]
            [clojure.data.xml :as xml]
            [medley.core :as med]))

(defn extract-string [n]
  (some-> n
          :content
          first
          str/trim))

(defn try-parse-long [s]
  (try
    #?(:clj (Long/parseLong s)
       :cljs (js/parseInt s))
    (catch Exception _
      nil)))

(defn extract-long [n]
  (some-> n
          :content
          first
          try-parse-long))

(defn extract-data [procs n]
  (when n
    (let [nodes (:content n)]
      (reduce
       (fn [acc {:keys [tag] :as node}]
         (if-let [pproc (procs tag)]
           (let [{:keys [label proc cardinality]} pproc]
             (case cardinality
               :one (assoc acc label (proc node))
               (update acc label #(if %1 (conj %1 %2) [%2]) (proc node))))
           acc))
       nil (filter map? nodes)))))

(defn add-map-ns [uri m]
  (med/map-keys (comp (partial xml/qname uri) name) m))
