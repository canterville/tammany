(ns io.gitlab.canterville.tammany-test
  (:require [clojure.test :refer :all :as test]
            [io.gitlab.canterville.tammany :refer :all :as tam]
            [clojure.spec.alpha :as s]
            [clojure.java.io :as io]
            [expound.alpha :as exp]))

(set! s/*explain-out* exp/printer)
(s/check-asserts true)

(deftest parse-feeds
  (doseq [feed-file (rest (file-seq (io/file "dev-resources/feeds")))]
    (testing (format "Test parsing feed %s" feed-file)
      (let [feed (-> feed-file (io/reader) (tam/process-feed))
            m (meta feed)]
        (is (map? (s/assert ::tam/feed feed)))
        (is (> (count (:entries feed)) 0))
        (is (= #{::tam/version ::tam/format ::tam/label ::tam/raw} (into #{} (keys m))))
        (is (map? (::tam/raw m)))))))
